class RPNCalculator
  attr_accessor :stack

  def initialize
    @stack = []
  end

  def push(*num)
    @stack.concat(num)
  end

  def pop
    @stack.delete_at(-1)
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def times
    perform_operation(:*)
  end

  def divide
    perform_operation(:/)
  end

  def value
    @stack.last
  end

  def tokens(string)
    scanned = string.scan(/\S+/)
    int = ('0'..'9').to_a
    scanned.map do |ch|
      int.include?(ch) ? Integer(ch) : ch.to_sym
    end
  end

  def evaluate(string)
    tokens = tokens(string)

    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end

    value
  end

  private

  def perform_operation(op_symbol)
    raise 'calculator is empty' if @stack.length < 2
    @second_num = @stack.pop.to_f
    @first_num = @stack.pop.to_f

    case op_symbol
    when :+
      @stack << @first_num + @second_num
    when :-
      @stack << @first_num - @second_num
    when :*
      @stack << @first_num * @second_num
    when :/
      @stack << @first_num / @second_num
    else
      @stack.push(@first_num)
      @stack.push(@second_num)
      raise 'invalid operation'
    end

  end

end
